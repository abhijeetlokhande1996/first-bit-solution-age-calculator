import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'AgeCalculator';
  selectedDob = null;
  selectedToDate = null;
  dataFromDateIpComponent(e) {
    console.table(e);
    this.selectedDob = e['dob'];
    this.selectedToDate = e['toDate'];
  }
}
