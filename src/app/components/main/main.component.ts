import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {
  dobArr: Array<Date> = [];
  @Output('dataFromDateIp') dataFromDateIp = new EventEmitter<{}>();
  constructor() {}

  ngOnInit(): void {
    this.dobArr = this.createDobArray();
  }
  createDobArray() {
    const dArr: Array<Date> = [];
    const startYear = 1990;
    const endYear = 1991;
    for (let year = startYear; year < endYear + 1; year++) {
      for (let month = 0; month < 12; month++) {
        for (let day = 1; day < 32; day++) {
          const d: Date = new Date(year, month, day);
          dArr.push(d);
        }
      }
    }
    return dArr;
  }
  getDataFromDateComponent(e) {
    console.table(e);
    this.dataFromDateIp.emit(e);
  }
}
