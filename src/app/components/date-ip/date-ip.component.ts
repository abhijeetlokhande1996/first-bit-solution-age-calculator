import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-date-ip',
  templateUrl: './date-ip.component.html',
  styleUrls: ['./date-ip.component.css'],
})
export class DateIpComponent implements OnInit {
  selectedDob: Date = null;
  selctedToDate: Date = null;
  //@Input('dobArray') dobArr: Array<Date>;
  dobArr: Array<Date>;

  @Input('dobArray')
  set dobArray(value) {
    this.dobArr = value;
  }
  @Output('userSelection')
  userSelection = new EventEmitter<{}>();

  toDateArr: Array<Date>;
  displayToDate = false;
  constructor() {}

  ngOnInit(): void {}
  onChangeDob() {
    this.toDateArr = this.creatrToDateArray(this.selectedDob);
    this.displayToDate = true;
  }
  creatrToDateArray(selectedDob) {
    const arr = [];
    const dobYear = new Date(selectedDob).getFullYear();
    for (let year = dobYear + 1; year < 1992; year++) {
      for (let month = 0; month < 12; month++) {
        for (let day = 1; day < 32; day++) {
          const d: Date = new Date(year, month, day);
          arr.push(d);
        }
      }
    }
    return arr;
  }
  calculateAge() {
    const obj = {
      dob: this.selectedDob,
      toDate: this.selctedToDate,
    };
    this.userSelection.emit(obj);
  }
}
