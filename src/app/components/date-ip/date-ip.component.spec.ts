import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateIpComponent } from './date-ip.component';

describe('DateIpComponent', () => {
  let component: DateIpComponent;
  let fixture: ComponentFixture<DateIpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateIpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateIpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
