import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { DateIpComponent } from './components/date-ip/date-ip.component';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [AppComponent, MainComponent, DateIpComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
